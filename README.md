# Quiz Website

This is a minimalistic web interface for real-time quizzes. It's written in PHP and uses a MySQL/MariaDB database.

[![Version](https://img.shields.io/badge/Version-1.0-success)](https://gitlab.com/s-heim/quiz-website/-/releases) 
[![MIT License](https://img.shields.io/badge/License-MIT-orange)](http://opensource.org/licenses/MIT) 


## Game Scenario

This application is designed for multiple users in a privat enviroment. There is no authentification process included! The website may be used in kind of following scenario:

The game master ask open questions. Each question has a specified number. The players can type in their answers into the form on the website. Before the quiz starts, each player initially typed in their name, which will be stored the whole session. The database stored the given name, the question number, the given answer and the current time. So the game master can rank the players manually, the possibility the get the given answers will be displayed, when the user name is "Admin".

In this context this website is only for fun in a scenario, where no security measures are needed.

## Run The Game

The quiz game can be deployed in various ways, for example in a [XAMPP](https://www.apachefriends.org/de/index.html)-Enviroment.

Be sure, to have created an empty database in your enviroment with available credentials. Before starting the application, take a look in the [config.php](source/config.php) and adjust the variables. The first four constants are for the database connection. The fifth defines the displayed quiz name. You can also use another favicon by replacing it in the [assets](source/website/assets)-folder. It must be used a picture in the ico-format. The quiz is designed in german language.

## License

[MIT © SH](LICENSE)

