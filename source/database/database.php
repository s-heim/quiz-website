<?php

    require_once '../config.php';
	
	class Database {
			
		private $conn = 'mysql:host=' . DBHOST . ';dbname=' . DBNAME;
		private $user = DBUSER;
		private $pass = DBPASS;
		private $pdo;
		
		public function connect() {
			if ($this->pdo != null) {
				return;
			}
			try {
				$this->pdo = new PDO($this->conn, $this->user, $this->pass);
				$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} 
			catch (PDOException $ex) {
				exit("DB Connection failed: " . $ex->getMessage());
			}
		}
		
		public function prepareStatement($sql) {
			if ($this->pdo == null) {
				$this->connect();
			}
			return $this->pdo->prepare($sql);
		}
			
		public function close() {
			if ($this->pdo == null) {
				return;
			}
			$this->pdo = null;
		}
		
	}
	
?>