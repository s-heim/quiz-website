DROP TABLE IF EXISTS `entries`;

CREATE TABLE IF NOT EXISTS `entries` (
  `EntryID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(255) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Entry` varchar(255) NOT NULL,
  PRIMARY KEY (`EntryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
