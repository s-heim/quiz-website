<?php
	
	if (!isset($_SESSION)) {
		session_start();
	}

	$message = null;

	require_once 'header.php';
	require_once '../database/database.php';

	if (isset($_POST['username'])) {	
		$_SESSION['UserName'] = $_POST['username'];
	}

	if (isset($_POST['logout'])) {	
		$_SESSION['UserName'] = null;
	}

	if (!isset($_SESSION['UserName'])) {
		require_once 'login.php'; 
		die ();
	}

	if (isset($_POST['qid']) && isset($_POST['entry'])) {	
        $db = new Database();
        $db->connect();
        $sql = "INSERT INTO entries (UserName, QuestionID, Date, Entry) VALUES (:user, :qid, NOW(), :entry)";
		$statement = $db->prepareStatement($sql);
		$statement->bindValue(':user', $_SESSION['UserName']);
		$statement->bindValue(':qid', $_POST['qid']);
        $statement->bindValue(':entry', $_POST['entry']);
		$statement->execute();
		$db->close();
		$message = "<span class='text-success'>Eintrag erfolgreich gespeichert!</span>";
	}
	
?>

<div class="p-2 m-2">
	<form action="index.php" method="post">
		<input type="hidden" name="logout" value=true />
		<p>Angemeldet als: <?php echo $_SESSION['UserName'] ?></p> <button type="submit" class="btn btn-outline-secondary btn-sm">Abmelden</button>
	</form>
</div>

<div class="p-2 m-2 border border-dark border-3">
	<h2>Frage beantworten</h2>
    <form action="index.php" method="post">
        <div class="mb-3">
            <label for="qid" class="form-label">Fragen-Nr.:</label>
            <input type="number" class="form-control" id="qid" name="qid" required />
        </div>
        <div class="mb-3">
            <label for="entry" class="form-label">Lösung:</label>
            <input type="text" class="form-control" id="entry" name="entry" required />
        </div>
        <button type="submit" class="btn btn-primary">Absenden</button>
		<?php echo $message ?>
    </form>
</div>

<?php if ($_SESSION['UserName'] == "Admin") { ?>

<div class="p-2 m-2">
	<h2>Auswertung</h2>
    <form action="summary.php" method="get">
        <div class="mb-3">
            <label for="qid" class="form-label">Fragen-Nr: </label>
            <input type="number" class="form-control" id="qid" name="qid" required/>
        </div>
        <button type="submit" class="btn btn-outline-secondary btn-sm">Frage auswerten</button>
    </form>
</div>

<?php } ?>


<?php require_once 'footer.php'; ?>