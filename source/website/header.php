<?php 
	require_once '../config.php'; 
?>

<!DOCTYPE html>

<html lang="en">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous" />
	<link rel="icon" type="image/x-icon" href="assets/favicon.ico">	
	<title><?php echo QUIZNAME; ?></title>
</head>
	
	
<body>

<div class="container-fluid">

<h1 class="m-2">Willkommen zum <?php echo QUIZNAME; ?> <img width="50" src="assets/favicon.ico" alt="logo" /></h1>