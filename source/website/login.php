<?php
	
	if (!isset($_SESSION)) {
		session_start();
	}
	if (isset($_SESSION['UserName'])) {
		require_once 'index.php'; 
		die ();
	}
	
?>

<div class="p-2 m-2">
    <form action="index.php" method="post">
        <div class="mb-3">
            <label for="username" class="form-label">Name eingeben:</label>
            <input type="text" class="form-control" id="username" name="username" required/>
        </div>
        <button type="submit" class="btn btn-primary">Absenden</button>
    </form>
</div>

<?php require_once 'footer.php'; ?>