<?php

    $entries = null;
	
	if (!isset($_SESSION)) {
		session_start();
	}

    require_once 'header.php'; 
    require_once '../database/database.php';

    if (isset($_GET['qid'])) {	
        $db = new Database();
        $db->connect();
        $sql = "SELECT * FROM entries WHERE QuestionID = :qid ORDER BY Date asc";
		$statement = $db->prepareStatement($sql);
		$statement->bindValue(':qid', $_GET['qid']);
		$statement->execute();
        $entries = $statement->fetchAll();
		$db->close();
	}

    if ($entries != null) {
        echo "<table class='table'><thead><tr><th scope='col'>Name</th><th scope='col'>Zeitstempel</th><th scope='col'>Lösung</th></tr></thead><tbody>";
        foreach($entries as $entry) {
            echo "<tr><td>";
            echo $entry['UserName'];
            echo "</td><td>";
            echo $entry['Date'];
            echo "</td><td>";
            echo $entry['Entry'];
            echo "</td></tr>";
        }
        echo "</tbody></table>";
    }
    else {
        echo "<p class='m-2 p-2'>Keine Antworten gefunden!</p>";
    }

?>

<div class="p-2 m-2">
	<form action="index.php" method="post">
		<button type="submit" class="btn btn-outline-secondary btn-sm">Zurück</button>
	</form>
</div>

<?php  require_once 'footer.php'; ?>
